"""
+-------------------------------------------------------------------------------+
|                                                                               |
|  Greenbox Bot - Version vom 2018.10.01                                        |
|                                                                               |
|  Der Greenbox Bot, wie wir ihn auf dem Discord “Greenbox Server” verwenden.   |
|                                                                               |
|                                                                               |
|  Copyright (C) 2017-2018 mydarkstar                                           |
|                                                                               |
|  This program is free software: you can redistribute it and/or modify         |
|  it under the terms of the GNU Affero General Public License as published by  |
|  the Free Software Foundation, either version 3 of the License, or            |
|  (at your option) any later version.                                          |
|                                                                               |
|  This program is distributed in the hope that it will be useful,              |
|  but WITHOUT ANY WARRANTY; without even the implied warranty of               |
|  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                |
|  GNU Affero General Public License for more details.                          |
|                                                                               |
|  You should have received a copy of the GNU Affero General Public License     |
|  along with this program.  If not, see https://www.gnu.org/licenses/.         |
|                                                                               |
|                                                                               |
|  Der folgende Code nutzt:                                                     |
|  1. Python 3.4.2+                                                             |
|  2. aiohttp>=1.0.0,v1.1.0                                                     |
|  3. websockets>=3.1,<4.0                                                      |
|  und discord.py ( https://github.com/Rapptz/discord.py ).                     |
|                                                                               |
|  WICHTIG:                                                                     |
|  Um den folgenden Code nutzen zu können, müssen IDs von Kanälen, Servern, etc.|
|  auf eigene abgeändert werden. Außerdem muss in einer der letzten Zeilen      |
|  das eigene Bot-Token eingetragen werden.                                     |
|  Hier kann ein neuer Discord Bot-Account + Token erstellt werden:             |
|  https://discordapp.com/developers/applications/                              |
|                                                                               |
|  Informationen zur Programmierung mit discord.py finden sich hier:            |
|  https://discordpy.readthedocs.io/en/latest/api.html                          |
|                                                                               |
+-------------------------------------------------------------------------------+
"""

import discord
import time
import asyncio
import random
import re
from datetime import datetime
import secret_vars

client = discord.Client()


# +------------------------------------------------------------+
# |                      VARIABLEN, ETC.                       |
# +------------------------------------------------------------+

# Einige Definitionen
commandPrefix = "!"
commandPrefixMusikbot1 = "!"
commandPrefixMusikbot2 = "!!"

# ID des Discord Servers
idServer = 239515010232090624

# IDs der Musikbots
idBotMusik1 = 279687439579086848
idBotMusik2 = 339160047122644993

# IDs der einzelnen Textkanäle
idChannelStartseite = 239515010232090624
idChannelRegeln = 241771675648131073
idChannelMusikbots = 279720912272621569
idChannelLaberfeuer = 326403678145609738
idChannelMinecraft = 243756451850354688
idChannelMod = 335370568696332289
idChannelVerification = 394301236687667201

channelStartseite = discord.Object(id=idChannelStartseite)
channelLaberfeuer = discord.Object(id=idChannelLaberfeuer)
channelMod = discord.Object(id=idChannelMod)
channelVerification = discord.Object(id=idChannelVerification)


# +------------------------------------------------------------+
# |                         LABERFEUER                         |
# +------------------------------------------------------------+

# Das Laberfeuer
async def laberfeuerclock():
    await client.wait_until_ready()
    alreadypurged = ((int(datetime.now().strftime('%H')) >= 5) & (int(datetime.now().strftime('%H')) < 21)) # Gibt an, ob das Laberfeuer bereits zurückgesetzt wurde
    while not client.is_closed:
        if (int(datetime.now().strftime('%H')) >= 5) & (int(datetime.now().strftime('%H')) < 21) & (not alreadypurged): # Zeit zwischen 5 - 21 Uhr
            await client.purge_from(channelLaberfeuer, limit=1000)
            await asyncio.sleep(30)
            await client.purge_from(channelLaberfeuer, limit=1000)
            await asyncio.sleep(10)
            await client.send_file(channelLaberfeuer, "files/laberfeuer.png") # Dateiupload
            await client.send_message(channelLaberfeuer, '**Das Laberfeuer ist momentan geschlossen**\nAlle Nachrichten, die während den Öffnungszeiten geschrieben wurden, sind jetzt verbrannt.\nDas Feuer wird um 21 Uhr wieder angezündet! 🔥')
            alreadypurged = True
        elif ((int(datetime.now().strftime('%H')) < 5) | (int(datetime.now().strftime('%H')) >= 21)) & (alreadypurged): # Zeit zwischen 21 - 5 Uhr
            await client.purge_from(channelLaberfeuer, limit=1000)
            await asyncio.sleep(2)
            await client.send_file(channelLaberfeuer, "files/laberfeuer.png") # Dateiupload
            await client.send_message(channelLaberfeuer, '**Das Laberfeuer wurde soeben wieder entzündet!**\nVon 21 - 5 Uhr kann jetzt in diesem Kanal geschrieben werden.\nAlle geschriebenen Nachrichten verbrennen um 5 Uhr morgens.\nPrivatere Nachrichten können serverweit mit einem $-Zeichen am Anfang versehen werden.\nDiese werden schon nach 10 Minuten gelöscht!\nUnd jetzt: viel Spaß! 🔥')
            
            # Zufällige "Erster!"-Nachricht nach zufälliger Zeit
            await asyncio.sleep(random.randint(30,200))
            randn = random.randint(1,12)
            if (randn == 3):
                msg = 'Erster Bot!'
            elif (randn == 7):
                msg = 'Allerallererster!'
            elif (randn == 9):
                msg = 'Zweiter!'
            else:
                randn = 0
            if (randn):
                await client.send_message(channelLaberfeuer, msg)
            alreadypurged = False
        await asyncio.sleep(10)


# +------------------------------------------------------------+
# |                     BENACHRICHTIGUNGEN                     |
# +------------------------------------------------------------+

# => Wenn jemand den Server betritt - Willkommensnachricht und Mod-Benachrichtigung
@client.event
async def on_member_join(member):
    server = member.server
    if (member.nick):
        usrName = '**' + str(member.nick) + '**'
    else:
        usrName = '**' + str(member.name) + '**'
    # Nachricht in #verification-Kanal
    await client.send_message(channelVerification, 'Hallöchen ' + member.mention + ', willkommen! Um mit dem Server interagieren zu können, musst du dich erst verifizieren.\nDieser Kanal hilft uns den Server und seine Mitglieder vor Spammern, Trolls & Co. zu schützen.\n\nUm verfiziert zu werden, lese bitte die Regeln in <#' + str(idChannelRegeln) + '> und schreibe kurz in diesen Kanal, wie du uns gefunden hast :)\n\n**Beispiel:** Hi, ich komme aus dem Twitch Stream und würde gerne hier mitmachen! :D\n\nDann viel Spaß noch, ein Mod wird dich schnellstens freischalten! <:gb:242773973635432450>')
    # Nachricht in #mod-Kanal
    await client.send_message(channelMod, '**Join:** ' + member.mention + ' (' + usrName + ') hat den Server betreten! Dieser Nutzer ist Gast und muss erst freigeschaltet werden.')


# => Wenn jemand den Server verlässt - Mod-Benachrichtigung und Abschiedsnachricht
@client.event
async def on_member_remove(member):
    server = member.server
    if (member.nick):
        usrName = '**' + str(member.nick) + '**'
    else:
        usrName = '**' + str(member.name) + '**'
    if (str(member.top_role) == '💚 Mitglied'):
        # Öffentliche Nachricht auf Verlassen auf der Startseite (optional):
        # await client.send_message(channelStartseite, 'Tschüss ' + usrName + '! Danke, dass du seit ' + member.joined_at.strftime('%A, %d.%m.%Y %H:%M:%S') + ' dabei warst!')
        await client.send_message(channelMod, '**Leave:** ' + member.mention + ' (' + usrName + ') [Mitglied] hat den Server verlassen!')
    else:
        await client.send_message(channelMod, '**Leave:** ' + member.mention + ' (' + usrName + ') [Gast] hat den Server verlassen!')



# => Wenn jemand verbannt wird - Mod-Benachrichtigung
@client.event
async def on_member_ban(member):
    server = member.server
    await client.send_message(channelMod, '**Ban:** {0.mention} wurde vom Server verbannt!'.format(member))


# => Wenn jemand entbannt wird - Mod-Benachrichtigung
@client.event
async def on_member_unban(member):
    server = member.server
    await client.send_message(channelMod, '*Unban:** {0.mention} wurde vom Server entbannt!'.format(member))



# => Wenn ein Mitglied geändert wird
@client.event
async def on_member_update(before, after):
    server = after.server
    if (after.nick):
        usrName = '**' + str(after.nick) + '**'
    else:
        usrName = '**' + str(after.name) + '**'
    if ((str(after.top_role) == '🚫 Text muted') & (str(before.top_role) !='🚫 Text muted')):
        await client.send_message(channelMod, '**Mute:** {0.mention} ist jetzt gemuted!'.format(after))
        await client.send_message(after, '**Du wurdest auf dem 0x00c Discord Server gemuted!**\nDies ist die Folge der Verwendung von Beleidigungen und bösen Wörtern und durch ein Verstoß gege die Regeln.\nDie Mute-Strafe dauert so lange an, wie wir es für richtig halten.\nBitte passe auf, dass du dich nächstes Mal besser verhälst!\n*Falls du denkst, dies ist ein Fehler eines Bots, kannst du uns gerne bescheid sagen.*')
    elif ((str(after.top_role) != '🚫 Text muted') & (str(before.top_role) == '🚫 Text muted')):
        await client.send_message(channelMod, '**Unmute:** {0.mention} ist nicht mehr gemuted!'.format(after))
        await client.send_message(after, '**Du wurdest auf dem 0x00c Discord Server wieder entmuted!**\nAber bitte halte dich immer an die Regeln und höre auf die Mods! :)')
    if ((str(after.top_role) == '🚫 Timeout') & (str(before.top_role) != '🚫 Timeout')):
        await client.send_message(channelMod, '**Timeout:** {0.mention} ist jetzt getimeouted!'.format(after))
        await client.send_message(after, '**Du wurdest auf dem 0x00c Discord Server getimeouted!**\nDies ist die Folge der Verwendung von Beleidigungen und bösen Wörtern und durch ein Verstoß gege die Regeln.\nDie Timeout-Strafe dauert so lange an, wie wir es für richtig halten.\nBitte passe auf, dass du dich nächstes Mal besser verhälst!')
    elif ((str(after.top_role) != '🚫 Timeout') & (str(before.top_role) == '🚫 Timeout')):
        await client.send_message(channelMod, '**Untimeout:** {0.mention} ist nicht mehr getimeouted!'.format(after))
        await client.send_message(after, '**Du wurdest auf dem 0x00c Discord Server wieder enttimeouted!**\nAber bitte halte dich immer an die Regeln und höre auf die Mods! :)')
    if ((str(after.top_role) == '💚 Mitglied') & (str(before.top_role) != '💚 Mitglied')):
        await client.send_message(channelMod, '**+Mitglied:** {0.mention} ist jetzt ein Mitglied!'.format(after))
        await client.send_message(channelStartseite, 'Hallöchen {0.mention}, willkommen auf dem 0x00c Discord Server! Du bist jetzt Mitglied, viel Spaß noch! =)'.format(after))
    elif ((str(after.top_role) != '💚 Mitglied') & (str(before.top_role) == '💚 Mitglied')):
        await client.send_message(channelMod, '**-Mitglied:** {0.mention} wurde die Mitgliedschaft entzogen!'.format(after))
    if (str(before.nick) != str(after.nick)):
        await client.send_message(channelMod, '**Nick:** ' + after.mention + ' hat den Nick von **' + str(before.nick) + '** -> **' + str(after.nick) + '** geändert.')
    if (str(before.name) != str(after.name)):
        await client.send_message(channelMod, '**Name:** ' + after.mention + ' hat den Namen von **' + str(before.name) + '** -> **' + str(after.name) + '** geändert.')


# +------------------------------------------------------------+
# |                          TOOLBOX                           |
# +------------------------------------------------------------+

# => Commandprüfung
def isCommand(commandTest, commandOriginal):
    # Ob empfangener Command "command(to)Test" dem erwarteten Command "commandOrigial" entspricht
    return ((commandTest == commandPrefix + commandOriginal) | commandTest.startswith(commandPrefix + commandOriginal + ' '))

# => Commandprüfung für Musikbot-Commands
def isMusikbotCommand(commandTest, commandOriginal):
    # Ob Command mit "[jeweiliger Prefix][commandOriginal]" anfängt
    return (commandTest.startswith(commandPrefixMusikbot1 + commandOriginal) | commandTest.startswith(commandPrefixMusikbot2 + commandOriginal))



# =========================================================================================================================================



# +------------------------------------------------------------+
# |            MAIN - VERARBEITUNG DER NACHRICHTEN             |
# +------------------------------------------------------------+

@client.event
async def on_message(message):
    
    # => Vorbereitung
    # Namen oder Nicknamen für weitere Benutzung vorbereiten
    msgLower = message.content.lower()
    if (message.author.nick):
        usrName = '**' + str(message.author.nick) + '**'
        usrNameRaw = str(message.author.nick)
    else:
        usrName = '**' + str(message.author.name) + '**'
        usrNameRaw = str(message.author.name)
    
    
    # => Generelle Überprüfungen
    if (message.author == client.user):
        return # Eigene Nachrichten sollen nicht empfangen werden

    if (message.channel.is_private):
        await client.send_message(message.channel, '**Es tut mir leid, aber dies ist ein privater Bot des Greenbox Community Discordservers!**\nIch kann dir daher Nachrichen in privaten Chats nicht beantworten! :(\nProbiere es mal hier: <http://0x00c.world/discord>'.format(message))
        return # Nachrichten aus Privatchats sollen nicht empfangen werden

    if (int(message.server.id) != idServer):
        await client.send_message(message.channel, '**Es tut mir leid, aber dies ist ein privater Bot des Greenbox Community Discordservers!**\nIch kann dir daher die Nachricht hier nicht beantworten! :(\nProbiere es mal hier: <http://0x00c.world/discord>'.format(message))
        return # Nachrichten von unbekannten Servern sollen nicht empfangen werden

    # => Wortfilter / Spamschutz
    # Verbotene Worte
    if (re.search( r'wichser', msgLower, re.M|re.I)):
        ##  Weniger schlimme Sachen -> [löschen und darauf hinweisen]
        await client.delete_message(message)
        msgToDelete = await client.send_message(message.channel, '{0.author.mention} **Bitte ändere deine Ausdrucksweise!**'.format(message))
        await asyncio.sleep(4)
        await client.delete_message(msgToDelete)
        return
    elif (re.search( r'hurensohn', msgLower, re.M|re.I)):
        ##  Schlimme Sachen -> [löschen und muten]
        await client.delete_message(message)
        await client.replace_roles(message.author, discord.utils.get(message.server.roles, name='Text muted')) # Muted-Rolle zuweisen
        msgToDelete = await client.send_message(message.channel, '{0.author.mention} **Bitte ändere deine Ausdrucksweise!**\nDu wurdest temporär gemuted.'.format(message))
        await asyncio.sleep(10)
        await client.delete_message(msgToDelete)
        return

    # => Mod-Tools
    if ((str(message.author.top_role) == '👨‍💻 Root Control Program') | (str(message.author.top_role) == '👨‍💻 Main Control Program') | (str(message.author.top_role) == '🥝 Kernel')):
        # Personen, die den Bot etwas "sagen" lassen könenn
        if (message.content.startswith(commandPrefix + "say ")):
            await client.delete_message(message)
            await client.send_message(message.channel, message.content.split(commandPrefix + "say ", 1)[1])
            return
        
        # Den ausgewählten Kanal der letzten 1000 Nachrichten bereinigen
        if (msgLower == commandPrefix + "purge"):
            try:
                await client.purge_from(message.channel, limit=1000)
            except Exception:
                await client.send_message(message.channel, "Whoops! Keine Nachrichten in den letzten 14 Tagen gefunden")
                pass
            return


    # => Musikbots & Musikkanal
    if (int(message.channel.id) == idChannelMusikbots):
        if (message.author.bot):
            if ((int(message.author.id) != idBotMusik1) & (int(message.author.id) != idBotMusik2)):
                # Nachrichten anderer Bots, die in den Musikkanal schreiben löschen
                await client.delete_message(message)
        
        # Normale Nachrichten, welche nicht mit einem Musikbot-Prefix anfangen, löschen
        elif (not (msgLower.startswith(commandPrefixMusikbot1) | msgLower.startswith(commandPrefixMusikbot2))):
            await client.delete_message(message)
            msgToDelete = await client.send_message(message.channel, "{0.author.mention}, in diesem Kanal sind leider nur Musikbot Commands erlaubt".format(message))
            await asyncio.sleep(4)
            await client.delete_message(msgToDelete)
        
        # Now Playing soll länger gezeigt werden
        elif (msgLower.startswith('!np') | msgLower.startswith('!nowplaying')):
            await asyncio.sleep(30)
            await client.delete_message(message)
        
        # Alle normalen Musikbot-Commands nach 4 Sekunden löschen
        else:
            await asyncio.sleep(4)
            await client.delete_message(message)
        return
    
    # Löschen von Musikbot-Commands in anderen Kanälen
    elif (isMusikbotCommand(msgLower, 'play') | isMusikbotCommand(msgLower, 'playlists') | isMusikbotCommand(msgLower, 'queue') | isMusikbotCommand(msgLower, 'remove') | isMusikbotCommand(msgLower, 'search') | isMusikbotCommand(msgLower, 'scsearch') | isMusikbotCommand(msgLower, 'shuffle') | isMusikbotCommand(msgLower, 'skip') | isMusikbotCommand(msgLower, 'forceskip') | isMusikbotCommand(msgLower, 'pause') | isMusikbotCommand(msgLower, 'repeat') | isMusikbotCommand(msgLower, 'skipto') | isMusikbotCommand(msgLower, 'stop') | isMusikbotCommand(msgLower, 'volume') | isMusikbotCommand(msgLower, 'settings') | isMusikbotCommand(msgLower, 'about')):
        msgToDelete = await client.send_message(message.channel, message.author.mention + ", bitte benutze diese Commands in <#" + str(idChannelMusikbots) + ">!")
        await asyncio.sleep(1)
        await client.delete_message(message)
        await asyncio.sleep(3)
        await client.delete_message(msgToDelete)
        return


    # Laberfeuer
    if (int(message.channel.id) == idChannelLaberfeuer):
        if (int(datetime.now().strftime('%H')) >= 5) & (int(datetime.now().strftime('%H')) < 21):
            await client.delete_message(message)
            msgToDelete = await client.send_message(message.channel, '**Das Laberfeuer wird erst ab 21 Uhr angezündet!**\nAlle geschriebenen Nachrichten verbrennen um 5 Uhr morgens in den Flammen.\nDann erlischt das Feuer auch, gute Nacht! =)'.format(message))
            await asyncio.sleep(8)
            await client.delete_message(msgToDelete)
            return
        elif (msgLower == '!länge'):
            msg = '8' + '=' * int(round((abs(hash(str(usrNameRaw))) % (10 ** 2))/2)) + 'D'
            await client.send_message(message.channel, msg)
            return

    # Minecraft channel
    if (int(message.channel.id) == idChannelMinecraft):
        if (msgLower == '!spieler'):
            await client.add_roles(message.author, discord.utils.get(message.server.roles, name='MC Server Mitglied'))
            await client.send_message(message.channel, "**Du hast dich als Spieler für den Minecraft Server angemeldet!**\nAb jetzt erhälst du in diesem Kanal News über den Server.")


    # Generelle commands
    if (msgLower == '!länge'):
        msgToDelete = await client.send_message(message.channel, message.author.mention + ", dieser Command ist nur im <#" + str(idChannelLaberfeuer) + "> verfügbar")
        await asyncio.sleep(4)
        await client.delete_message(message)
        await client.delete_message(msgToDelete)

    if (msgLower.startswith("ich mag züge")):
        msgToEdit = await client.send_message(message.channel, "```\n" + " "*29 + "🚂\n```")
        for i in range(1, 7):
            await asyncio.sleep(1)
            await client.edit_message(msgToEdit, "```\n" + " "*(29-(i*4)) + "🚂\n```")
        await client.edit_message(msgToEdit, "*WOOOOOSH!!*")

    if (isCommand(msgLower, 'bart')):
        nameNum = int(round((abs(hash(str(usrNameRaw))) % (10 ** 2))/5))
        msgs = {
            1: "*Not bad!*",
            2: "*Schick! ;)*",
            3: "*Uh, baby!*",
            4: "*Sexy!*",
            5: "*Hot!*",
            6: "*u on fire, my boi!*",
            7: "*Damn gurl! :D*",
            8: "*Holy beard!*",
            9: "*Dang, it's Santa!*",
            10: "*Look at dis!*",
            11: "*Es ist Oma Gerda!*",
            12: "*Look at what you've got!"
        }
        await client.send_message(message.channel, "🧔"*nameNum + " " + msgs.get(nameNum, "*Like a grandpa!*"))
    
    
    
    # => Autodelete messages
    if (msgLower.startswith('$')):
        await asyncio.sleep(600)
        try:
            await client.delete_message(message)
        except Exception:
            pass
        return
    
    
    
    # => GIF commands
    if (isCommand(msgLower, 'nati')):
        msgs = {
            1: 'https://media.giphy.com/media/GHycyakNPWSoo/giphy.gif',
            2: 'https://media.giphy.com/media/3o7TKPATxjC1zfIwW4/200w_d.gif',
            3: 'https://media.giphy.com/media/bi1dfmrAxEGyc/200w_d.gif',
            4: 'https://media.giphy.com/media/jzw9G5Yvi2PXW/200w_d.gif',
            5: 'https://media.giphy.com/media/VtRW9YZLtIyzK/200w_d.gif',
            6: 'https://media.giphy.com/media/1dPTVv6FaQmZ2/200w_d.gif',
            7: 'https://media.giphy.com/media/l4FGKD7DgAxW0Uc2k/200w_d.gif',
            8: 'https://media.giphy.com/media/LX3Q9vcjwhKYE/200w_d.gif',
            9: 'https://media.giphy.com/media/ZTfTSegFNMnC0/200w_d.gif',
            10: 'https://media.giphy.com/media/Whjig4hIs5xtu/200w_d.gif',
            11: 'https://media.giphy.com/media/ZTfTSegFNMnC0/200w_d.gif',
            12: 'https://media.giphy.com/media/Az8jmEa1Bc9zi/200w_d.gif'
        }
        await client.send_message(message.channel, msgs.get(random.randint(1,12), ""))

    if (isCommand(msgLower, 'trump')):
        msgs = {
            1: 'https://media.giphy.com/media/oxsfuzJuJzCjm/giphy.gif',
            2: 'https://media.giphy.com/media/wJNGA01o1Zxp6/giphy.gif',
            3: 'https://media.giphy.com/media/l3V0bNrtqFVbD4pVu/giphy.gif',
            4: 'https://media.giphy.com/media/3oKIPf1BaBDILVxbYA/giphy.gif',
            5: 'https://media.giphy.com/media/Qjmp5vKEERPyw/giphy.gif',
            6: 'https://media.giphy.com/media/znrnkBB5sKfhS/giphy.gif',
            7: 'https://media.giphy.com/media/xT8qBvVrX0wuuItpFm/giphy.gif',
            8: 'https://media.giphy.com/media/l46CAPzswmb3qjGgg/giphy.gif',
            9: 'https://media.giphy.com/media/pBEiJbZrR7PrO/giphy.gif',
            10: 'https://media.giphy.com/media/d2ZbaUxwOa0jLCBW/giphy.gif',
            11: 'https://giphy.com/gifs/3o7TKEbIr6Cco4NWGQ/html5',
            12: 'https://media.giphy.com/media/11Up43rPtFBneU/giphy.gif'
        }
        await client.send_message(message.channel, msgs.get(random.randint(1,12), ""))

    if (isCommand(msgLower, 'rainbow')):
        msgs = {
            1: 'https://media.giphy.com/media/6p26sp0YT2LAI/giphy.gif',
            2: 'https://media.giphy.com/media/26AHG5KGFxSkUWw1i/giphy.gif',
            3: 'https://media.giphy.com/media/Qjmp5vKEERPyw/giphy.gif', # trump-rolled
            4: 'https://media.giphy.com/media/3oz8xDp5mAEOAZXEPe/giphy.gif',
            5: 'https://media.giphy.com/media/xThuWcaa4U4XZQDgvm/giphy.gif',
            6: 'https://media.giphy.com/media/H7F0zSaC9ZHZC/giphy.gif',
            7: 'https://media.giphy.com/media/CMaHXgP5bY00g/giphy.gif',
            8: 'https://media.giphy.com/media/SKGo6OYe24EBG/giphy.gif',
            9: 'https://media.giphy.com/media/xT1XGZndeDLlWvSDaU/giphy.gif',
            10: 'https://media.giphy.com/media/3XdsWf4oqSZ6E/giphy.gif',
            11: 'https://media.giphy.com/media/xTiTnIdG44UV8Lkl4A/giphy.gif',
            12: 'https://media.giphy.com/media/f5GyIBXJ3L0DS/giphy.gif'
        }
        await client.send_message(message.channel, msgs.get(random.randint(1,12), ""))


    if (msgLower.startswith('ping')):
        msg = 'pong' * msgLower.count('ping')
        await client.send_message(message.channel, msg)

    if (isCommand(msgLower, 'herein')):
        if (message.author.voice.voice_channel):
            await client.send_message(message.channel, '**>>** ' + usrName + ' lädt alle in den Sprachkanal **{0.author.voice.voice_channel.name}** ein!'.format(message))
        else:
            msgToDelete = await client.send_message(message.channel, '{0.author.mention}, du bist in keinem Sprachkanal!'.format(message))
            await asyncio.sleep(4)
            await client.delete_message(message)
            await client.delete_message(msgToDelete)

    if (isCommand(msgLower, 'time')):
        await client.send_message(message.channel, 'Du bist schon dabei seit: ' + message.author.joined_at.strftime('%A, %d.%m.%Y %H:%M:%S'))
    
    
    
    # => Smaller commands
    if (isCommand(msgLower, 'dance')):
        msgs = {
            1: '♫ **{0.author.mention}**, lass uns tanzen! ♪ ┏(-\\_-)┛┗(-\\_- )┓┗(-\\_-)┛┏(-\\_-)┓',
            2: '♫ **{0.author.mention}**, lass uns tanzen! ♪ └(^ㅁ^)┐ ┌(^ㅁ^)┘ └(^ㅁ^)┐ ┌(^ㅁ^)┘',
            3: '♫ **{0.author.mention}**, lass uns tanzen! ♪ <(^ㅁ^)/ ＼(^ㅁ^)> <(^ㅁ^)/ ＼(^ㅁ^)>'
        }
        await client.send_message(message.channel, msgs.get(random.randint(1,3)).format(message))
    
    
    if (isCommand(msgLower, 'kuscheln')):
        await client.send_message(message.channel, '{0.author.mention} möchte **kuscheln**!'.format(message))

    elif (isCommand(msgLower, 'greenboxliebe')):
        await client.send_message(message.channel, '{0.server.member_count}x # GREENBOXLIEBE! ❤ ❤ ❤'.format(message))

    elif (isCommand(msgLower, 'spendlove')):
        await client.send_message(message.channel, usrName + ' spendet liebe in den Chat ' + '❤ '*random.randint(3,5))

    elif (isCommand(msgLower, 'herzen')):
        await client.send_message(message.channel, '❤ ' * random.randint(4,20))

    elif (isCommand(msgLower, 'liebe')):
        randn = random.randint(3,5)
        await client.send_message(message.channel, '❤ '*randn + usrName + ', Love is in the Air ' + '❤'*randn)

    elif (isCommand(msgLower, 'stream')):
        await client.send_message(message.channel, 'Jeden Samstag ab 19:00 Uhr tun wir coole Dinge! Ein besonders großes Dankeschön an alle Follower, ihr seid die Besten!!! Updates gibts übrigens immer auf Twitter: <https://twitter.com/0x00c>')

    elif (isCommand(msgLower, 'andre')):
        await client.send_message(message.channel, 'Auch bekannt als \'Speicherblock\' und \'0x00c\', geht er jeden Samstag um 19€ **höchst professionell** auf <https://twitch.tv/0x00c> live.\nWir haben wohl ein Date? 😉')

    elif (isCommand(msgLower, 'danke')):
        await client.send_message(message.channel, 'vielen vielen Dank!!! 😄 -- ❤ x 1000')

    elif (isCommand(msgLower, 'facepalm')):
        await client.send_message(message.channel, '(－‸ლ)')

    elif (isCommand(msgLower, 'flip')):
        await client.send_message(message.channel, '(╯°□°）╯︵ ┻━┻')

    elif (isCommand(msgLower, 'groupdance')):
        await client.send_message(message.channel, '(/・・)ノ     (ﾉ･ｪ･)ﾉ     (ﾉ*ﾟｰﾟ)ﾉ')

    elif (isCommand(msgLower, 'jackbox')):
        await client.send_message(message.channel, '**Bei den Jackbox Spielen könnt ihr hier mitspielen:**\n<http://jackbox.tv>')

    elif (isCommand(msgLower, 'help') | isCommand(msgLower, 'hilfe')):
        await client.send_message(message.channel, 'Schau mal in <#' + str(idChannelRegeln) + '> und <#305424997613043712> rein :)')

    elif (isCommand(msgLower, 'markup')):
        await client.send_message(message.channel, '```\nFett: **fett**\nKursiv: *kursiv* _kursiv_\nDurchgestrichen: ~~Durchgestrichen~~\n```')

    elif (isCommand(msgLower, 'nice')):
        await client.send_message(message.channel, '**Jeder** hier hat die Haare schön!' + ' :D'*random.randint(0,1))

    elif (isCommand(msgLower, 'lenny')):
        await client.send_message(message.channel, '( ͡° ͜ʖ ͡°)')

    elif (isCommand(msgLower, 'look')):
        await client.send_message(message.channel, 'ಠ_ಠ')

    elif (isCommand(msgLower, 'pfeif')):
        await client.send_message(message.channel, 'ヾ( ͝° ͜ʖ͡°)ノ♪')

    elif (isCommand(msgLower, 'shrug')):
        await client.send_message(message.channel, '¯\_(ツ)_/¯')

    elif (isCommand(msgLower, 'scatman')):
        await client.send_message(message.channel, 'SKI' + 'BI'*random.randint(2,5) + 'BA'*random.randint(1,3) + 'DA'*random.randint(1,2) + 'BODADAD' + 'A'*random.randint(1,4) + '!\n**I\'M A SCATMAN' + '!'*random.randint(2,5) + '**\nBADADADABABAB' + 'I'*random.randint(2,5) + 'BA'*random.randint(2,5) + 'DABUMM!')

    elif (isCommand(msgLower, 'goodmorning')):
        randn = random.randint(1,2)
        if (randn == 1):
            await client.send_message(message.channel, '*Ein neuer Tag bricht an* 🌅\nGuuuten Morgen, ihr Morningboxen! =)')
        elif (randn == 2):
            await client.send_message(message.channel, '*Ein neuer Tag bricht an* 🌄\nGuuuten Morgen, ihr Morningboxen! =)')

    elif (isCommand(msgLower, 'sleepwell')):
        await client.send_message(message.channel, 'Gute Nacht euch allen da draußen!\nLove y\'all! ' + '❤ '*random.randint(1,3))

    elif (isCommand(msgLower, 'hammertime')):
        randn = random.randint(1,3)
        await client.send_message(message.channel, '👉'*randn + ' Can\'t touch this ' + '👈'*randn)

    elif (isCommand(msgLower, 'tee')):
        await client.send_message(message.channel, '*Schüttet dir Tee ein* :)')

    elif (isCommand(msgLower, 'kaffee')):
        await client.send_message(message.channel, '**COFFEE TIME!** :D\n*Schenkt dir einen Pott Käffken ein! :D*')

    elif (isCommand(msgLower, 'mehrtee')):
        await client.send_message(message.channel, '*Schüttet dir eine EXTRA GROSSE Tasse Tee ein!* :)' + '\n' + 'http://www.gearfuse.com/wp-content/uploads/2007/12/mug-cat.jpg')

    elif (isCommand(msgLower, 'kartoffelsupp')):
        await client.send_message(message.channel, '*Kartoffelsupp einschütt :\'D*')

    elif (isCommand(msgLower, 'thunder')):
        await client.send_message(message.channel, 'YOU\'VE BEEN................\n**THUNDERSTRUCK\'D!!!!!!!!!!!!!!!!!!!!!!!!!!!**\n' + '⚡'*random.randint(6,16))

    elif (isCommand(msgLower, 'wow')):
        await client.send_message(message.channel, 'Ich bin begeistert! 😮')

    elif (isCommand(msgLower, 'muffinparty')):
        await client.send_message(message.channel, '<:muffin:281199156629078016>'*random.randint(12,36))

    elif (isCommand(msgLower, 'toplel')):
        msgToDelete = await client.send_message(message.channel, '░░░░░░░░░░░░▄▀▀▌\n░░░░░░░▄▀█░░█▄▀\n░░░▄▄▀▐▌░▐▌░█░░░▄▄▀▀▌\n░▀▀█░░█▄▄▀░░▄▄▀▀▌▄█▌▌\n░░░█░░░░░▄▀▀▄▄▐▒▌███▐\n░░░▀░░░▐▀▄█▀▀▒▒▒▌███▐\n░░▄▀▀▌░▐▐██▌▒▄▐▒▌███▐▀▐\n▐▀▄█▌▌░▐▐█████▐▒▌███▄█▐\n▐▐██▌▌░▐▐███▀▒▒▒▌████▀▐\n▐▐██▌▌░▐▐███▒▄▐▒▌█▀▄▄▀▀\n▐▐██▌▌▀▐▐█████▐▒▄▄▀\n▐▐██▌▄█▐▐█▀▀▄▄▀▀\n▐▐████▀▐▄▄▀▀\n▐▐█▀▀▄▀▀\n▐▄▄▀ ')
        await asyncio.sleep(10)
        await client.delete_message(message)
        await client.delete_message(msgToDelete)

    elif (isCommand(msgLower, 'troll')):
        msgToDelete= await client.send_message(message.channel, '░░░░▄▄▄▄▀▀▀▀▀▀▀▀▄▄▄▄▄▄\n░░░░█░░░░▒▒▒▒▒▒▒▒▒▒▒▒░░▀▀▄\n░░░█░░░▒▒▒▒▒▒░░░░░░░░▒▒▒░░█\n░░█░░░░░░▄██▀▄▄░░░░░▄▄▄░░░█\n░▀▒▄▄▄▒░█▀▀▀▀▄▄█░░░██▄▄█░░░█\n█▒█▒▄░▀▄▄▄▀░░░░░░░░█░░░▒▒▒▒▒█\n█▒█░█▀▄▄░░░░░█▀░░░░▀▄░░▄▀▀▀▄▒█\n░█▀▄░█▄░█▀▄▄░▀░▀▀░▄▄▀░░░░█░░█\n░░█░░▀▄▀█▄▄░█▀▀▀▄▄▄▄▀▀█▀██░█\n░░░█░░██░░▀█▄▄▄█▄▄█▄████░█\n░░░░█░░░▀▀▄░█░░░█░███████░█\n░░░░░▀▄░░░▀▀▄▄▄█▄█▄█▄█▄▀░░█\n░░░░░░░▀▄▄░▒▒▒▒░░░░░░░░░░█\n░░░░░░░░░░▀▀▄▄░▒▒▒▒▒▒▒▒▒▒░█\n░░░░░░░░░░░░░░▀▄▄▄▄▄░░░░░█\n\n                                  Problem? :D')
        await asyncio.sleep(10)
        await client.delete_message(message)
        await client.delete_message(msgToDelete)

    elif (isCommand(msgLower, 'poopy')):
        await client.send_message(message.channeel, 'EINHORNPOOPY!')

    elif (isCommand(msgLower, 'muffintime')):
        await client.send_message(message.channel, '**It\'s MUFFINTIIIIME! <:muffin:281199156629078016>' + '\n' + 'https://cdn.discordapp.com/attachments/239515010232090624/332602282241687552/muffintime.gif')




    """Template
    elif (isCommand(msgLower, '')):
        await client.send_message(message.channel, '')

    """


@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

client.loop.create_task(laberfeuerclock())
client.run(secret_vars.token)



"""ASCII

# +------------------------------------------------------------+
# |                              
# +------------------------------------------------------------+
"""
